#!/usr/bin/env python3
import telebot
import configparser
import setproctitle
import random
from tinydb import TinyDB, Query, where
from tinydb_smartcache import SmartCacheTable
from telebot import util
import sys
import traceback

DELETEADVICE = 'Пришли, сука, id эдвайзов для удаления.'
NEWADVICE = 'Пришли, блять, новый эдвайз.'
ADVICEATTACH = 'Пишли, блять, id эдвайза, к которому хочешь что-то прихуярить'
ADVICEATTACH_2 = 'Что ты, блять, хочешь прихуярить к эдвайзу'
SEARCHADVICE = 'Что ищем, бля?'

config = configparser.ConfigParser()
config.read('bot.cfg')

TOKEN = config['bot']['token']
ADMINS = list(map(int, config['bot']['admins'].split(', ')))

bot = telebot.TeleBot(TOKEN)
db = TinyDB('db.json')
Advice = Query()
advices = db.table('advices')
pers_advices = db.table('pers_advices')
advices.table_class = SmartCacheTable

def admin(func):
    def func_wrapper(message, **kwargs):
        if message.from_user.id in ADMINS:
            func(message, **kwargs)
    return func_wrapper

@bot.message_handler(commands=['advice'])
def advice(message):
    if message.from_user.id == 111238197:
        bot.send_message(message.chat.id, 'Не тупи, блять.')
        bot.send_sticker(message.chat.id, 'BQADAgADzQADGnHrAxY1i6mu6EieAg')
        return
    result = None
    if len(advices) > 0:
        while not result:
            result = advices.get(eid=random.randint(0, advices._last_id))
    else:
        result = {'text': 'Заполни блять базу советов'}
    if 'pic' in result:
        bot.send_photo(message.chat.id, result['pic'], caption=result['text'])
    elif 'audio' in result:
        bot.send_audio(message.chat.id, result['audio'], caption=result['text'])
    else:
        bot.send_message(message.chat.id, result['text'])
    if 'sticker' in result:
        bot.send_sticker(message.chat.id, result['sticker'])

@bot.message_handler(commands=['advice_search', 'search', 's'])
@admin
def advice_search_pre(message):
    bot.send_message(message.chat.id, SEARCHADVICE, reply_markup=telebot.types.ForceReply())

@bot.message_handler(func=lambda m: m.reply_to_message and m.reply_to_message.text == SEARCHADVICE and m.text)
@admin
def advice_search(message):
    advice_list(message, query=message.text.lower())
 
@bot.message_handler(commands=['advice_list', 'list', 'ls'])
@admin
def advice_list(message, query=''):
    result = ''
    if query:
        query = query.lower()
        advs = advices.search(Advice.text.test(lambda text: query in text.lower()))
    else:
        advs = advices.all()
    for advice in advs:
        result = '{result}(\u274c:/del_{eid}, \u2795:/att_{eid}) {text}\n'.format(result=result, eid=advice.eid, text=advice['text'])
    result = util.split_string(result, 3000)
    for text in result:
        bot.send_message(message.chat.id, text)

@bot.message_handler(commands=['advice_delete', 'delete', 'del'])
@admin
def advice_delete_pre(message):
    bot.send_message(message.chat.id, DELETEADVICE, reply_markup=telebot.types.ForceReply())

@bot.message_handler(regexp='^/del_[0-9]*$')
@bot.message_handler(func=lambda m: m.reply_to_message and m.reply_to_message.text == DELETEADVICE and m.text)
@admin
def advice_delete(message):
    if '/del_' in message.text:
        eids = [int(message.text.replace('/del_', ''))]
    else:
        try:
            eids = list(map(int, message.text.split()))
        except ValueError:
            bot.send_message(message.chat.id, 'Бля, что я непонятного сказал?')
            return
    advices.remove(eids=eids)
    bot.send_message(message.chat.id, 'Удалил, ёпта. Если они вообще были, конечно.')

@bot.message_handler(commands=['advice_add', 'add', 'a'])
@admin
def advice_add_pre(message):
    bot.send_message(message.chat.id, NEWADVICE, reply_markup=telebot.types.ForceReply())    

@bot.message_handler(func=lambda m: m.reply_to_message and m.reply_to_message.text == NEWADVICE and m.text)
@admin
def advice_add(message):
    advices.insert({'text': message.text})
    bot.send_message(message.chat.id, 'Запомнил, бля.')

@bot.message_handler(commands=['advice_attach', 'attach'])
@admin
def advice_attach_pre(message):
    bot.send_message(message.chat.id, ADVICEATTACH, reply_markup=telebot.types.ForceReply())

@bot.message_handler(regexp='^/att_[0-9]*$')
@bot.message_handler(func=lambda m: m.reply_to_message and m.reply_to_message.text == ADVICEATTACH and m.text)
@admin
def advice_attach_pre_num(message):
    if '/att_' in message.text:
        eid = int(message.text.relplace('/att_', ''))
    else:
        try:
            eid = int(message.text)
        except ValueError:
            bot.send_message(message.chat.id, 'Это по-твоему похоже на ёбаный id?!')
            return
    if advices.get(eid=eid):
        bot.send_message(message.chat.id, '{} {}?'.format(ADVICEATTACH_2, eid), reply_markup=telebot.types.ForceReply())
    else:
        bot.send_message(message.chat.id, 'Нету, блять, такого id!')

@bot.message_handler(func=lambda m: m.reply_to_message and m.reply_to_message.text.startswith(ADVICEATTACH_2), content_types=['audio', 'sticker', 'photo'])
@admin
def advice_attach(message):
    try:
        eid = int(message.reply_to_message.text.split()[-1][0:-1])
    except:
        bot.send_message(message.chat.id, 'Что, блять?')
        return
    attached = False
    if message.audio:
        advices.update({'audio': message.audio.file_id}, eids=[eid])
        attached = True
    elif message.sticker:
        advices.update({'sticker': message.sticker.file_id}, eids=[eid])
        attached = True
    elif message.photo:
        advices.update({'pic': message.photo[0].file_id}, eids=[eid])
        attached = True
    if attached:
        bot.send_message(message.chat.id, 'Прихуярил, блять.')
    else:
        bot.send_message(message.chat.id, 'Ты че мне, блять, прислал? Как я это тебе, блять, прихуярю?')

if __name__ == "__main__":
    setproctitle.setproctitle('vasyanbot')
    while True:
        try:
            bot.polling(none_stop=True)
        except KeyboardInterrupt:
            sys.exit(0)
        except:
            try:
                print(traceback.format_exc())
            except:
                pass

